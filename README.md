# Maritime Container Shippment Platform

## Container Shipment User Interface

### Build & Run

This project is being updated to use the [Appsody](https://appsody.dev/) development framework. For a full understanding of Appsody applications, reference the Appsody [documentation](https://appsody.dev/docs) and [getting started](https://appsody.dev/docs/getting-started/) material respectively.

Specific deployment parameters will be exposed in the `app-deploy.yaml` file for the [User Interface](/master/app-deploy.yaml) microservice.

For complete documentation on the necessary deployment configuration and parameters, reference the **[User Interface](#)** microservice page in the [reference implementation](#) gitbook.